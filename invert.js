function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert

  let newObject = {}
  if (typeof obj == "object" || Object.keys(obj).length > 0) {
    for (let key in obj) {
        newObject = {...newObject, [obj[key]]: key}
        // newObject[obj[key]] = key
    }
    return newObject
  }
}

module.exports = invert
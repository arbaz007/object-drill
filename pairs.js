function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
  let result = [];
  if (typeof obj == "object" || Object.keys(obj).length > 0) {
    result = Object.entries(obj)
  }
  return result;
}

module.exports = pairs;

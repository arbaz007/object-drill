function keys(obj, result=[]) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  if (typeof obj == "object" || Object.keys(obj).length > 0) {
    for (let key in obj) {
      if (typeof obj[key] !== "object") {
        result.push(key);
      } else {
        result.push(key)
        keys(obj[key], result);
      }
    }
  }
  return result
}


module.exports = keys
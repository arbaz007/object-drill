function values(obj, ans = []) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values

  if (typeof obj == "object" || Object.keys(obj).length > 0) {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        values(obj[key], ans)
      }else {
        ans.push(obj[key])
      }
    }
  }
  return ans
}


module.exports = values

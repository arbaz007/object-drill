function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults

  if (typeof obj == "object" || Object.keys(obj).length > 0) {
    if (typeof defaultProps == "object") {
      for (key in defaultProps) {
        if (!Object.keys(obj).includes(key)) {
          obj = { ...obj, [key]: defaultProps[key] };
        }
      }
    }
  }
  console.log(obj);
}

module.exports = defaults;
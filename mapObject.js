function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    if (typeof obj == "object" || Object.keys(obj).length > 0) {
        for (let key in obj) {
            if (typeof obj[key] != "object") {
                obj[key] = cb (obj[key], key)
            }else {
                mapObject(obj[key], cb)
            }
        }
    }
    console.log(obj);
}

let o = {one: 1, two: 2, three: 3};
mapObject (o, (val, key) => {
    return val +5
})


module.exports = mapObject